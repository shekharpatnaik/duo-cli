# Duo CLI

Duo is a developer centric CLI that uses AI to help developers get productive quickly

## Command Set

Note the `debug` flag can be added to any command to display the LLM conversations.

Additionally, Duo supports profiles that lets you work with multiple LLMs and models. In order to switch a profile for a particular command, provide the `--profile` flag.


### Config

Configure your CLI with the details of your LLM. Current Duo support vertex and OpenAI.

You can also create multiple combinations using the `--profile` flag and then pass that flag to a specific command.

```sh
duo config set llm openai
duo config set api_key <YOUR API KEY>
duo config set model gpt-3.5-turbo 
duo config set model gpt-4 --profile gpt4
```

### Unit Tests

The `unittest` command creates unit tests based on a file. It will also run the unit test to make sure that if works. If not then it will send the error message to the LLM.

```sh
duo unittest create ./path/to/file
```

### Explain

Duo can explain code to you:

```sh
duo explain app.py
```

You can also pipe stdin into duo to explain code or an error message

```sh
python main.py 2>&1 | duo explain
```

### Review

Duo can review the code that has been created and optionally generate a patch file.

```sh
duo review --patch-file test.diff app.py
```

### Generate

Duo can generate files based on your file tree

```sh
duo generate Makefile
```

### Doc Strings

Duo can generate doc strings for your code

```sh
duo gen-doc app.py
```

### Convert

Duo can convert code from one language to another.

```sh
duo convert --lang=python myapp.bas
```

## Development

To build the code run

```
make
```

To run this code

```
./bin/duo
```