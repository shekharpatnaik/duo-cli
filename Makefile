.PHONY: build test

build: mkdir-bin
	go build -o ./bin/duo

mkdir-bin:
	mkdir -p ./bin

test:
	go test -v ./...