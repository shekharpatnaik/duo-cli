package config

import (
	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/config"
	"gitlab.com/duo/pkg/utils"
)

var SetCmd = &cobra.Command{
	Use:   "set",
	Short: "Set a specific config",
	Long:  "Set a specific config",
	Args:  cobra.ExactArgs(2),
	RunE: func(cmd *cobra.Command, args []string) error {
		err := config.Set(utils.GetProfile(cmd), args[0], args[1])
		if err != nil {
			return err
		}

		return nil
	},
}
