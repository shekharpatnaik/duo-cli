package config

import (
	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/config"
	"gitlab.com/duo/pkg/utils"
)

var LsCmd = &cobra.Command{
	Use:     "list",
	Aliases: []string{"ls"},
	Short:   "List the config",
	Long:    "List the config",
	RunE: func(cmd *cobra.Command, args []string) error {
		err := config.List(utils.GetProfile(cmd))
		if err != nil {
			return err
		}

		return nil
	},
}
