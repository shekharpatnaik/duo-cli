package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/prompt"
	"gitlab.com/duo/pkg/utils"
)

func init() {
	explainCmd.Flags().BoolVarP(&noFormat, "no-format", "n", false, "disable formatting")
	rootCmd.AddCommand(explainCmd)
}

var explainCmd = &cobra.Command{
	Use:   "explain",
	Short: "explain code",
	Long:  "explain code",
	RunE: func(cmd *cobra.Command, args []string) error {

		var data []byte
		var err error
		var filepath string

		if len(args) == 0 {
			data, err = utils.ReadFromStdin()
		} else {
			data, err = os.ReadFile(args[0])
			filepath = args[0]
		}
		if err != nil {
			return err
		}

		err = utils.ExecuteAgent(cmd, prompt.ExplainPrompt, map[string]string{
			"code":     string(data),
			"filepath": filepath,
			"tools":    prompt.ToolsPrompt,
		}, []agent.PipelineStep{
			agent.NewDisplayContentsStep(!noFormat),
		}, []agent.Tool{
			agent.NewReadFileTool(),
			agent.NewExecuteCommandTool(),
		})
		return err
	},
}
