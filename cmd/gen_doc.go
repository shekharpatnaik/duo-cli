package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/prompt"
	"gitlab.com/duo/pkg/utils"
)

func init() {
	rootCmd.AddCommand(genDocCmd)
}

var genDocCmd = &cobra.Command{
	Use:   "gen-doc",
	Short: "Generate docs for the code",
	Long:  "Generate docs for the code",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		// Read File
		data, err := os.ReadFile(args[0])
		if err != nil {
			return err
		}

		err = utils.ExecuteAgent(cmd, prompt.GenDocPrompt, map[string]string{
			"code":     string(data),
			"filepath": args[0],
		}, []agent.PipelineStep{
			agent.NewWriteFileStep(),
		}, []agent.Tool{})
		return err
	},
}
