package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/fs"
	"gitlab.com/duo/pkg/prompt"
	"gitlab.com/duo/pkg/utils"
)

func init() {
	rootCmd.AddCommand(generateCmd)
}

var generateCmd = &cobra.Command{
	Use:     "generate",
	Aliases: []string{"gen"},
	Short:   "Generate a file based on information provided",
	Long:    "Generate a file based on information provided",
	RunE: func(cmd *cobra.Command, args []string) error {
		// Create a tree of file arguments
		tree, err := fs.GenerateTree(".")
		if err != nil {
			return err
		}

		err = utils.ExecuteAgent(cmd, prompt.GeneratePrompt, map[string]string{
			"tree":     tree,
			"filepath": args[0],
		}, []agent.PipelineStep{
			agent.NewWriteFileStep(),
		}, []agent.Tool{})
		return err
	},
}
