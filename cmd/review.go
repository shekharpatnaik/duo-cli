package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/prompt"
	"gitlab.com/duo/pkg/utils"
)

var patchFile string

func init() {
	rootCmd.AddCommand(reviewCmd)
	reviewCmd.Flags().StringVar(&patchFile, "patch-file", "", "Path where the patch file should be written")
}

var reviewCmd = &cobra.Command{
	Use:   "review",
	Short: "Review code",
	Long:  "Review code and present results",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		// Read File
		data, err := os.ReadFile(args[0])
		if err != nil {
			return err
		}

		err = utils.ExecuteAgent(cmd, prompt.ReviewPrompt, map[string]string{
			"code":     string(data),
			"filepath": args[0],
		}, []agent.PipelineStep{
			agent.NewDisplayAndApplyReviewStep(),
			agent.NewWritePatchFileStep(patchFile),
		}, []agent.Tool{})
		return err
	},
}
