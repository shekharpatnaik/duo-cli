package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/prompt"
	"gitlab.com/duo/pkg/utils"
)

var language string

func init() {
	rootCmd.AddCommand(ConvertCmd)
	ConvertCmd.Flags().StringVarP(&language, "lang", "l", "", "Language to convert to")
	ConvertCmd.MarkFlagRequired("lang")
}

var ConvertCmd = &cobra.Command{
	Use:   "convert",
	Short: "Convert the code to another language",
	Long:  "Convert the code to another language",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		// Read File
		data, err := os.ReadFile(args[0])
		if err != nil {
			return err
		}

		err = utils.ExecuteAgent(cmd, prompt.ConvertPrompt, map[string]string{
			"code":     string(data),
			"language": language,
			"filepath": args[0],
		}, []agent.PipelineStep{
			agent.NewWriteFileStep(),
		}, []agent.Tool{})
		return err
	},
}
