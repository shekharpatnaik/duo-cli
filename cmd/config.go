package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/duo/cmd/config"
)

func init() {
	rootCmd.AddCommand(configCmd)
	configCmd.AddCommand(config.SetCmd)
	configCmd.AddCommand(config.LsCmd)
}

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Configure details for your LLM",
	Long:  "Configure details for your LLM",
}
