package unittest

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/prompt"
	"gitlab.com/duo/pkg/utils"
)

func init() {
	CreateCmd.Flags().Bool("dry-run", false, "Is this a dry run")
}

var CreateCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a unit test based on a file provided",
	Long:  "Create a unit test based on a file provided",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {

		// Read File
		data, err := os.ReadFile(args[0])
		if err != nil {
			return err
		}

		err = utils.ExecuteAgent(cmd, prompt.UnitTestCreatePrompt, map[string]string{
			"code":     string(data),
			"filepath": args[0],
		}, []agent.PipelineStep{
			agent.NewWriteFileStep(),
			agent.NewExecuteCommandStep(),
		}, []agent.Tool{})
		if err != nil {
			return err
		}

		return nil
	},
}
