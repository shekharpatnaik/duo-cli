package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/config"
	"gitlab.com/duo/pkg/llm"
	"gitlab.com/duo/pkg/utils"
)

var (
	noFormat bool
)

func init() {
	rootCmd.AddCommand(chatCmd)
	chatCmd.Flags().BoolVarP(&noFormat, "no-format", "n", false, "Disable formatting of output")
}

var chatCmd = &cobra.Command{
	Use:   "chat",
	Short: "Chat with the LLM",
	Long:  "Chat with the LLM",
	RunE: func(cmd *cobra.Command, args []string) error {
		ctx := context.Background()

		// Get config
		config, err := config.Get(utils.GetProfile(cmd))
		if err != nil {
			return err
		}

		model, err := llm.Factory(config)
		if err != nil {
			return err
		}

		tools := []agent.Tool{
			agent.NewTreeTool(),
			agent.NewReadFileTool(),
			agent.NewSaveFileTool(),
			agent.NewExecuteCommandTool(),
		}

		startingPrompt, err := utils.GenerateToolsPrompt(tools)
		if err != nil {
			return err
		}

		agt := agent.NewChatAgent(model, tools, !noFormat)

		err = agt.Run(ctx, startingPrompt)
		return err
	},
}
