package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/log"
)

var rootCmd = &cobra.Command{
	Use:   "duo",
	Short: "Duo is a CLI to improve your developer exprience with AI",
	Long:  "Duo helps improve your developer experience with AI",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

var debug bool

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "debug chat messages")
	rootCmd.PersistentFlags().String("profile", "default", "Which profile to use")
}

func initConfig() {
	log.Debug(debug)
}
