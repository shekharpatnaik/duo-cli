package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/duo/cmd/unittest"
)

func init() {
	rootCmd.AddCommand(unittestCmd)
	unittestCmd.AddCommand(unittest.CreateCmd)
}

var unittestCmd = &cobra.Command{
	Use:   "unittest",
	Short: "Add Unit Tests to your project",
	Long:  "Add Unit Tests to your project",
}
