package cmd

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	duofs "gitlab.com/duo/pkg/fs"
	"gitlab.com/duo/pkg/prompt"
	"gitlab.com/duo/pkg/utils"
)

var (
	drawingType string
)

func init() {
	drawCmd.Flags().StringVarP(&drawingType, "type", "t", "ER", "Type of drawing you want. Examples are sequence, ER, flowchart, class, state, etc")
	rootCmd.AddCommand(drawCmd)
}

var drawCmd = &cobra.Command{
	Use:   "draw",
	Short: "Draw a diagram from the code",
	Long:  "Draw a diagram from the code",
	RunE: func(cmd *cobra.Command, args []string) error {

		var files []file

		if len(args) == 0 {
			data, err := utils.ReadFromStdin()
			if err != nil {
				return err
			}
			files = append(files, file{
				Name: "stdin",
				Data: data,
			})
		} else {
			// Check if file is directory
			info, err := os.Stat(args[0])
			if err != nil {
				return err
			}

			if !info.IsDir() {
				data, err := os.ReadFile(args[0])
				if err != nil {
					return err
				}

				files = append(files, file{
					Name: args[0],
					Data: data,
				})
			} else {
				// TODO: Will create a large prompt, need to see what we can reduce.
				err = filepath.WalkDir(args[0], func(path string, d fs.DirEntry, err error) error {

					if _, ok := duofs.IgnoreDirectories[d.Name()]; ok {
						return filepath.SkipDir
					}

					if d.IsDir() {
						return nil
					}

					data, err := os.ReadFile(path)
					if err != nil {
						return err
					}

					files = append(files, file{
						Name: path,
						Data: data,
					})

					return nil
				})
				if err != nil {
					return err
				}
			}
		}

		err := utils.ExecuteAgent(cmd, prompt.DrawPrompt, map[string]string{
			"type":  drawingType,
			"files": format(files),
		}, []agent.PipelineStep{
			agent.NewDisplayContentsStep(false),
		}, []agent.Tool{})

		return err
	},
}

type file struct {
	Name string
	Data []byte
}

func format(filelist []file) string {
	var result string
	for _, file := range filelist {
		result = fmt.Sprintf("%sFilename:%s```%s```\n", file.Name, string(file.Data))
	}
	return result
}
