package log

import (
	"strings"

	"github.com/fatih/color"
	"gitlab.com/duo/pkg/llm"
)

var debug = false

func Debug(d bool) {
	debug = d
}

func Write(memory []*llm.Message) {
	if debug {
		latestMessage := memory[len(memory)-1]

		actorFormatting := color.New(color.Bold, color.FgHiYellow).PrintfFunc()
		actorFormatting("%s: ", strings.ToUpper(string(latestMessage.Role)))
		color.White("%s", latestMessage.Text)
	}
}
