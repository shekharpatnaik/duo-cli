package utils

import (
	"context"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/config"
	"gitlab.com/duo/pkg/llm"
	"gitlab.com/duo/pkg/prompt"
)

func ExecuteAgent(cmd *cobra.Command, promptTemplate string, templateInput map[string]string, steps []agent.PipelineStep, tools []agent.Tool) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	// Get config
	config, err := config.Get(GetProfile(cmd))
	if err != nil {
		return err
	}

	// Construct LLM message
	prompt, err := prompt.Generate(promptTemplate, templateInput)
	if err != nil {
		return err
	}

	// Send message to LLM
	model, err := llm.Factory(config)
	if err != nil {
		return err
	}

	agent := agent.NewPipelineAgent(model, steps, tools)
	err = agent.Run(ctx, prompt)
	if err != nil {
		return err
	}

	return nil
}
