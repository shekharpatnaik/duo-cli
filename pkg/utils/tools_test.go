package utils

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/duo/pkg/agent"
)

func TestGenerateToolsPrompt(t *testing.T) {
	tools := []agent.Tool{
		agent.NewReadFileTool(),
		agent.NewSaveFileTool(),
	}

	res, err := GenerateToolsPrompt(tools)
	require.NoError(t, err)

	require.Contains(t, res, "RUN TOOL READ")
	require.Contains(t, res, "RUN TOOL WRITE")
}
