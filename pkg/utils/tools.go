package utils

import (
	"bytes"
	"text/template"

	"gitlab.com/duo/pkg/agent"
	"gitlab.com/duo/pkg/prompt"
)

func GenerateToolsPrompt(tools []agent.Tool) (string, error) {
	var toolsDescription []string
	for _, t := range tools {
		toolsDescription = append(toolsDescription, t.Description())
	}

	t, err := template.New("prompt").Parse(prompt.ChatToolsPrompt)
	if err != nil {
		return "", err
	}

	res := bytes.NewBuffer([]byte(""))

	err = t.Execute(res, toolsDescription)
	if err != nil {
		return "", err
	}

	return res.String(), nil
}
