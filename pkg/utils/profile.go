package utils

import "github.com/spf13/cobra"

func GetProfile(cmd *cobra.Command) string {
	profile, err := cmd.Flags().GetString("profile")
	if err != nil || profile == "" {
		profile = "default"
	}

	return profile
}
