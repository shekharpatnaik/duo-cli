package utils

import (
	"bytes"
	"io"
	"os"
)

func ReadFromStdin() ([]byte, error) {
	outBuf := bytes.NewBuffer([]byte{})
	_, err := io.Copy(outBuf, os.Stdin)
	if err != nil {
		return nil, err
	}

	return outBuf.Bytes(), nil
}
