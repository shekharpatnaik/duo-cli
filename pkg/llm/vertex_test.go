package llm

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/duo/pkg/config"
)

func TestVertexChatCompletion(t *testing.T) {

	creds := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	if creds == "" {
		t.Skip("skipping test; GOOGLE_APPLICATION_CREDENTIALS not set")
	}

	llm, err := NewVertex(&config.Config{
		Temperature: 0,
		Project:     os.Getenv("GCP_PROJECT"),
		Model:       "chat-bison@001",
	})

	require.NoError(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*120)
	defer cancel()

	res, err := llm.GetCompletion(ctx, []*Message{{
		Role: "user",
		Text: "what does this python code do? `print('Hi')`",
	}})

	require.NoError(t, err)

	require.NotEmpty(t, res.Text)
	require.NotEmpty(t, res.Role)
}
