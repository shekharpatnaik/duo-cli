package llm

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.com/duo/pkg/config"
)

type GLChat struct {
	pat          string
	url          string
	project      string
	sentMessages []*Message
}

func NewGLChat(cfg *config.Config) (*GLChat, error) {

	if cfg.Project == "" {
		return nil, fmt.Errorf("project is required in config")
	}

	if cfg.ApiKey == "" {
		return nil, fmt.Errorf("api_key is required in config")
	}

	if cfg.InstanceUrl == "" {
		return nil, fmt.Errorf("instance_url is required in config")
	}

	return &GLChat{
		pat:     cfg.ApiKey,
		url:     cfg.InstanceUrl,
		project: fmt.Sprintf("gid://gitlab/Project/%s", cfg.Project),
	}, nil
}

func (l *GLChat) GetCompletion(ctx context.Context, messages []*Message) (*Message, error) {

	// Get text of last message
	input := messages[len(messages)-1].Text

	req, err := l.getMutationRequest(ctx, input)
	if err != nil {
		return nil, err
	}

	client := http.Client{}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	bodyBytes, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("error calling api. Status code: %d, Error: %s", res.StatusCode, string(bodyBytes))
	}

	var response glMutationResponse
	err = json.Unmarshal(bodyBytes, &response)
	if err != nil {
		return nil, err
	}

	requestId := response.Data.AIAction.RequestId
	if requestId == "" {
		return nil, fmt.Errorf("error calling api. Status code: %d, Error: %s", res.StatusCode, string(bodyBytes))
	}

	for {
		req, err := l.getQueryRequest(ctx, requestId)
		if err != nil {
			return nil, err
		}

		res, err := client.Do(req)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()

		bodyBytes, err := io.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}

		if res.StatusCode != 200 {
			return nil, fmt.Errorf("error calling api. Status code: %d, Error: %s", res.StatusCode, string(bodyBytes))
		}

		var response glQueryResponse
		err = json.Unmarshal(bodyBytes, &response)
		if err != nil {
			return nil, err
		}

		if len(response.Data.AIMessages.Nodes) > 1 {
			responseMessage := &Message{
				Role: RoleAssistant,
				Text: response.Data.AIMessages.Nodes[1].Content,
			}

			l.sentMessages = append(l.sentMessages)
			return responseMessage, nil
		}

		time.Sleep(1000 * time.Millisecond)
	}
}

func (l *GLChat) getMutationRequest(ctx context.Context, input string) (*http.Request, error) {
	var gqlRequest = fmt.Sprintf(`
	mutation startNewChat($content: String!) {
		aiAction(input: { chat: { resourceId: "%s", content: $content } }) {
		  requestId
		  errors
		}
	  }
	`, l.project)

	request := map[string]interface{}{
		"query": gqlRequest,
		"variables": map[string]string{
			"content": input,
		},
	}

	reqBytes, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/graphql", l.url), bytes.NewBuffer(reqBytes))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+l.pat)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	req = req.WithContext(ctx)

	return req, nil
}

func (l *GLChat) getQueryRequest(ctx context.Context, requestId string) (*http.Request, error) {
	var gqlRequest = `
	query getAiMessages($requestIds: [ID!], $roles: [AiMessageRole!]) {
		aiMessages(requestIds: $requestIds, roles: $roles) {
		  nodes {
			requestId
			role
			content
			contentHtml
			timestamp
			errors
		  }
		}
	  }
	`

	request := map[string]string{
		"query":     gqlRequest,
		"variables": fmt.Sprintf(`{"requestIds": ["%s"], "roles": ["USER", "ASSISTANT", "SYSTEM"]}`, requestId),
	}

	reqBytes, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/api/graphql", l.url), bytes.NewBuffer(reqBytes))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+l.pat)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	req = req.WithContext(ctx)

	return req, nil
}

type glMutationResponse struct {
	Data glMutationResponseData `json:"data"`
}

type glMutationResponseData struct {
	AIAction glMutationResponseAIAction `json:"aiAction"`
}

type glMutationResponseAIAction struct {
	RequestId string   `json:"requestId"`
	Errors    []string `json:"errors"`
}

type glQueryResponse struct {
	Data glQueryResponseData `json:"data"`
}

type glQueryResponseData struct {
	AIMessages glQueryResponseAIMessages `json:"aiMessages"`
}

type glQueryResponseAIMessages struct {
	Nodes []glQueryResponseAIMessage `json:"nodes"`
}

type glQueryResponseAIMessage struct {
	RequestId   string   `json:"requestId"`
	Role        string   `json:"role"`
	Content     string   `json:"content"`
	ContentHtml string   `json:"contentHtml"`
	Timestamp   string   `json:"timestamp"`
	Errors      []string `json:"errors"`
}
