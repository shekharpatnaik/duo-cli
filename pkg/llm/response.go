package llm

import (
	"encoding/json"
	"regexp"
	"strings"
)

func ParseJSONResponse[T any](llmResponse string) (*T, error) {
	resp := cleanResponse(llmResponse)

	var result T
	err := json.Unmarshal([]byte(resp), &result)
	if err != nil {
		return nil, err
	}
	return &result, err
}

func cleanResponse(llmResponse string) string {
	if !strings.Contains(llmResponse, "```json") &&
		!strings.Contains(llmResponse, "```JSON") &&
		!strings.Contains(llmResponse, "{") {

		return llmResponse
	}

	r1 := regexp.MustCompile("(?s)```(?i)json(.+)```")
	matches := r1.FindStringSubmatch(llmResponse)

	if len(matches) > 1 {
		return matches[1]
	}

	r2 := regexp.MustCompile("(?s)\\{.*\\}")
	matches = r2.FindStringSubmatch(llmResponse)

	if len(matches) > 0 {
		return matches[0]
	}

	return llmResponse
}
