package llm

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseJSONResponse(t *testing.T) {
	tt := []struct {
		description     string
		input           string
		expectError     bool
		expectedContent string
	}{
		{
			description:     "When invalid JSON is passed returns error",
			input:           "invalid json",
			expectError:     true,
			expectedContent: "",
		},
		{
			description:     "When valid JSON is passed returns json",
			input:           "{ \"contents\": \"test\" }",
			expectError:     false,
			expectedContent: "test",
		},
		{
			description: "When json is passed in markdown syntax, parses json",
			input: `This is the answer
			` + "```json" + `
				{ 
					"contents": "test" 
				}
			` + "```",
			expectError:     false,
			expectedContent: "test",
		},
		{
			description: "When JSON is passed in markdown syntax, parses json",
			input: `This is the answer
			` + "```JSON" + `
				{ 
					"contents": "test" 
				}
			` + "```",
			expectError:     false,
			expectedContent: "test",
		},
		{
			description: "When JSON is found somewhere in the body, parses json",
			input: `This is the answer
				{ 
					"contents": "test"
				}
			`,
			expectError:     false,
			expectedContent: "test",
		},
	}

	for _, test := range tt {
		t.Run(test.description, func(t *testing.T) {
			resp, err := ParseJSONResponse[Example](test.input)
			if test.expectError {
				require.Error(t, err)
				return
			}

			require.NoError(t, err)
			require.Equal(t, test.expectedContent, resp.Contents)
		})
	}
}

type Example struct {
	Contents string `json:"contents"`
}
