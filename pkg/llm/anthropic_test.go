package llm

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/duo/pkg/config"
)

func TestAnthropicChatCompletion(t *testing.T) {

	apiKey := os.Getenv("ANTHROPIC_API_KEY")
	if apiKey == "" {
		t.Skip("skipping test; ANTHROPIC_API_KEY not set")
	}

	llm, err := NewAnthropic(&config.Config{
		Temperature: 0,
		ApiKey:      apiKey,
		Model:       "claude-2",
	})

	require.NoError(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*120)
	defer cancel()

	res, err := llm.GetCompletion(ctx, []*Message{{
		Role: "user",
		Text: "what does this python code do? `print('Hi')`",
	}})

	require.NoError(t, err)

	require.NotEmpty(t, res.Text)
	require.NotEmpty(t, res.Role)
}
