package llm

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/duo/pkg/config"
)

type anthropic struct {
	apiKey      string
	model       string
	temperature float32
}

const (
	anthropicChatCompletionsEndpoint = "https://api.anthropic.com/v1/complete"
)

func NewAnthropic(cfg *config.Config) (*anthropic, error) {
	if cfg.Model == "" {
		return nil, errors.New("model not found in config")
	}

	if cfg.ApiKey == "" {
		return nil, errors.New("api_key not found in config")
	}

	return &anthropic{
		apiKey:      cfg.ApiKey,
		model:       cfg.Model,
		temperature: cfg.Temperature,
	}, nil
}

func (o *anthropic) GetCompletion(ctx context.Context, messages []*Message) (*Message, error) {

	var msgs string
	for _, m := range messages {
		msgs = fmt.Sprintf("%s\n\n%s: %s", msgs, convertToAnthropicRole(m.Role), m.Text)
	}

	msgs = fmt.Sprintf("%s\n\nAssistant:", msgs)

	anthropicRequest := anthropicChatCompletionRequest{
		Model:             o.model,
		Prompt:            msgs,
		Temperature:       o.temperature,
		TopK:              1,
		MaxTokensToSample: 4096,
	}

	requestBytes, err := json.Marshal(&anthropicRequest)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest(http.MethodPost, anthropicChatCompletionsEndpoint, bytes.NewBuffer(requestBytes))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("anthropic-version", "2023-06-01")
	request.Header.Add("x-api-key", fmt.Sprintf("%s", o.apiKey))
	request.Header.Add("Accept", "application/json")

	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	respBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return nil, fmt.Errorf(
			"error received from the anthropic response: Status Code: %d, Message: %s",
			resp.StatusCode,
			string(respBytes),
		)
	}

	anthropicChatResponse := anthropicChatCompletionResponse{}

	err = json.Unmarshal(respBytes, &anthropicChatResponse)
	if err != nil {
		return nil, err
	}

	return &Message{
		Role: RoleAssistant,
		Text: anthropicChatResponse.Completion,
	}, nil
}

type anthropicChatCompletionRequest struct {
	Model             string  `json:"model"`
	Prompt            string  `json:"prompt"`
	Temperature       float32 `json:"temperature"`
	TopK              int     `json:"top_k"`
	MaxTokensToSample int     `json:"max_tokens_to_sample"`
}

type anthropicChatCompletionResponse struct {
	Model      string `json:"model"`
	Completion string `json:"completion"`
	StopReason string `json:"stop_reason"`
}

func convertToAnthropicRole(role Role) string {
	switch role {
	case RoleUser:
		return "Human"
	case RoleAssistant:
		return "Assistant"
	default:
		return "Human"
	}
}
