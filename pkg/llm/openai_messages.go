package llm

type openAIChatCompletionRequest struct {
	Model       string          `json:"model"`
	Messages    []openAIMessage `json:"messages"`
	Temperature float32         `json:"temperature"`
}

type openAIMessage struct {
	Role    string `json:"role"`
	Content string `json:"content"`
}

type openAIChatCompletionResponse struct {
	Choices []openAIChatCompletionResponseChoice `json:"choices"`
	Id      string                               `json:"id"`
}

type openAIChatCompletionResponseChoice struct {
	Message openAIMessage `json:"message"`
}
