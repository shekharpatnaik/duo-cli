package llm

import (
	"context"
	"fmt"

	aiplatform "cloud.google.com/go/aiplatform/apiv1"
	"cloud.google.com/go/aiplatform/apiv1/aiplatformpb"
	"gitlab.com/duo/pkg/config"
	"google.golang.org/api/option"
	"google.golang.org/protobuf/types/known/structpb"
)

type vertex struct {
	model       string
	temperature float32
	project     string
}

func NewVertex(cfg *config.Config) (LLM, error) {
	return &vertex{
		model:       cfg.Model,
		temperature: cfg.Temperature,
		project:     cfg.Project,
	}, nil
}

func (v *vertex) GetCompletion(ctx context.Context, messages []*Message) (*Message, error) {
	c, err := aiplatform.NewPredictionClient(ctx, option.WithEndpoint("us-central1-aiplatform.googleapis.com:443"))
	if err != nil {
		return nil, err
	}
	defer c.Close()

	params, err := structpb.NewValue(map[string]interface{}{
		"temperature":     v.temperature,
		"maxOutputTokens": 1024,
		"topK":            1,
		"topP":            0.8,
	})
	if err != nil {
		return nil, err
	}

	var vmessages []interface{}
	for _, m := range messages {
		vmessage := map[string]interface{}{
			"author":  string(m.Role),
			"content": m.Text,
		}

		vmessages = append(vmessages, vmessage)
	}

	instance, err := structpb.NewValue(map[string]interface{}{
		"context":  "You are a software expert. Please help the user solve their problem. Could you please format your responses in valid JSON as requested. Could you please encode the newline characters in the contents field of the messages as \\n?",
		"messages": vmessages,
	})
	if err != nil {
		return nil, err
	}

	req := &aiplatformpb.PredictRequest{
		Endpoint:   fmt.Sprintf("projects/%s/locations/us-central1/publishers/google/models/%s", v.project, v.model),
		Instances:  []*structpb.Value{instance},
		Parameters: params,
	}

	resp, err := c.Predict(ctx, req)
	if err != nil {
		return nil, err
	}

	respMap := resp.Predictions[0].GetStructValue().AsMap()
	content := respMap["candidates"].([]interface{})[0].(map[string]interface{})["content"]

	return &Message{
		Role: "assistant",
		Text: content.(string),
	}, nil
}
