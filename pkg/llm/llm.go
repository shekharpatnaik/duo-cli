package llm

import (
	"context"
	"errors"

	"gitlab.com/duo/pkg/config"
)

type LLM interface {
	GetCompletion(ctx context.Context, messages []*Message) (*Message, error)
}

type Role string

const (
	RoleUser      Role = "user"
	RoleAssistant Role = "assistant"
	RoleSystem    Role = "system"
)

type Message struct {
	Role Role   `json:"role"`
	Text string `json:"text"`
}

var (
	ErrUnknownLLM = errors.New("unknown LLM")
)

func Factory(cfg *config.Config) (LLM, error) {
	switch cfg.LLM {
	case "openai":
		return NewOpenAI(cfg)
	case "vertex":
		return NewVertex(cfg)
	case "anthropic":
		return NewAnthropic(cfg)
	case "gitlab":
		return NewGLChat(cfg)

	default:
		return nil, ErrUnknownLLM
	}
}
