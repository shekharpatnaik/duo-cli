package llm

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/duo/pkg/config"
)

type openai struct {
	apiKey      string
	model       string
	temperature float32
}

const (
	chatCompletionsEndpoint = "https://api.openai.com/v1/chat/completions"
)

func NewOpenAI(cfg *config.Config) (*openai, error) {
	if cfg.Model == "" {
		return nil, errors.New("model not found in config")
	}

	if cfg.ApiKey == "" {
		return nil, errors.New("api_key not found in config")
	}

	return &openai{
		apiKey:      cfg.ApiKey,
		model:       cfg.Model,
		temperature: cfg.Temperature,
	}, nil
}

func (o *openai) GetCompletion(ctx context.Context, messages []*Message) (*Message, error) {

	var msgs []openAIMessage
	for _, m := range messages {
		msgs = append(msgs, openAIMessage{
			Content: m.Text,
			Role:    string(m.Role),
		})
	}

	openAIRequest := openAIChatCompletionRequest{
		Model:       o.model,
		Messages:    msgs,
		Temperature: o.temperature,
	}

	requestBytes, err := json.Marshal(&openAIRequest)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest(http.MethodPost, chatCompletionsEndpoint, bytes.NewBuffer(requestBytes))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Authorization", fmt.Sprintf("Bearer %s", o.apiKey))
	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	respBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode >= 400 {
		return nil, fmt.Errorf("error received from the OpenAI response: Status Code: %d", resp.StatusCode)
	}

	openAIChatResponse := openAIChatCompletionResponse{}

	err = json.Unmarshal(respBytes, &openAIChatResponse)
	if err != nil {
		return nil, err
	}

	if len(openAIChatResponse.Choices) == 0 {
		return nil, fmt.Errorf("No choices returned from OpenAI API")
	}

	return &Message{
		Role: Role(openAIChatResponse.Choices[0].Message.Role),
		Text: openAIChatResponse.Choices[0].Message.Content,
	}, nil
}
