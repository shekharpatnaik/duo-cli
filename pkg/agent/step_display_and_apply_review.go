package agent

import (
	"fmt"
	"strings"

	"github.com/fatih/color"
	"gitlab.com/duo/pkg/prompt"
)

type displayAndApplyReview struct{}

func NewDisplayAndApplyReviewStep() PipelineStep {
	return &displayAndApplyReview{}
}

func (s *displayAndApplyReview) Execute(response *prompt.ResponseStruct) error {
	for _, comment := range response.Review {
		fmt.Printf("%s %s\n",
			color.YellowString("%d", comment.LineNumber),
			color.BlueString("%s", strings.Trim(comment.LineText, " ")))

		spaces := calculateSpaces(comment.LineNumber)

		color.Red("%s ^", spaces)
		color.Red("%s %s", spaces, comment.Comment)
		fmt.Println()
	}

	return nil
}

func calculateSpaces(lineNumber int) string {
	s := ""
	for i := 0; i < len(fmt.Sprintf("%d", lineNumber)); i++ {
		s += " "
	}
	return s
}
