package agent

import "context"

type Agent interface {
	Run(ctx context.Context, startingPrompt string) error
}
