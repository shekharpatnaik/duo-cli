package agent

import (
	"fmt"
	"os/exec"
	"strings"

	"gitlab.com/duo/pkg/prompt"
)

type executeCommandTool struct{}

func NewExecuteCommandTool() Tool {
	return &executeCommandTool{}
}

func (t *executeCommandTool) Check() string {
	return "RUN TOOL EXEC (.+)"
}

func (t *executeCommandTool) Description() string {
	return prompt.ExecToolPrompt
}

func (t *executeCommandTool) Run(params ...string) (string, error) {
	commands := strings.Split(params[0], " ")
	res, err := exec.Command(commands[0], commands[1:]...).CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("error in output of running command: Error %s\n %s", err, res)
	}

	return fmt.Sprintf("Output of command:\n%s", res), nil
}
