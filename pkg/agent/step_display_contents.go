package agent

import (
	"fmt"

	markdown "github.com/MichaelMure/go-term-markdown"
	"gitlab.com/duo/pkg/prompt"
)

type displayContents struct {
	displayMarkdown bool
}

func NewDisplayContentsStep(displayMarkdown bool) PipelineStep {
	return &displayContents{
		displayMarkdown: displayMarkdown,
	}
}

func (s *displayContents) Execute(response *prompt.ResponseStruct) error {
	if s.displayMarkdown {
		fmt.Println(string(markdown.Render(string(response.Contents), 80, 6)))
		return nil
	}

	fmt.Println(response.Contents)
	return nil
}
