package agent

type Tool interface {
	Description() string
	Check() string
	Run(params ...string) (string, error)
}
