package agent

import (
	"fmt"
	"os"

	"gitlab.com/duo/pkg/prompt"
)

type readFileTool struct{}

func NewReadFileTool() Tool {
	return &readFileTool{}
}

func (t *readFileTool) Description() string {
	return prompt.ReadToolPrompt
}

func (t *readFileTool) Check() string {
	return "RUN TOOL READ (.+)"
}

func (t *readFileTool) Run(params ...string) (string, error) {
	contentBytes, err := os.ReadFile(params[0])
	if err != nil {
		return "", err
	}

	contents := string(contentBytes)

	if len(string(contentBytes)) > 10000 {
		contents = fmt.Sprintf("%s\nFile is too long\n", contentBytes[:1000])
	}

	return contents, nil
}
