package agent

import (
	"os"
	"path"

	"gitlab.com/duo/pkg/prompt"
)

type writePatchFile struct {
	filename string
}

func NewWritePatchFileStep(filename string) PipelineStep {
	return &writePatchFile{filename}
}

func (s *writePatchFile) Execute(response *prompt.ResponseStruct) error {
	if s.filename == "" {
		return nil
	}

	// Write File
	err := os.MkdirAll(path.Dir(s.filename), os.ModePerm)
	if err != nil {
		return err
	}

	err = os.WriteFile(s.filename, []byte(response.Contents), os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}
