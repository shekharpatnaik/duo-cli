package agent

import (
	"context"
	"fmt"
	"regexp"

	"gitlab.com/duo/pkg/llm"
	"gitlab.com/duo/pkg/log"
	"gitlab.com/duo/pkg/prompt"
)

const (
	maxTurns = 10
)

type pipelineAgent struct {
	llm   llm.LLM
	steps []PipelineStep
	tools []Tool
}

func NewPipelineAgent(llm llm.LLM, steps []PipelineStep, tools []Tool) *pipelineAgent {
	return &pipelineAgent{
		llm:   llm,
		steps: steps,
		tools: tools,
	}
}

func (a *pipelineAgent) Run(ctx context.Context, startingPrompt string) error {
	memory := []*llm.Message{
		{
			Role: "user",
			Text: startingPrompt,
		},
	}

	var rounds int32
	for rounds = 1; rounds <= maxTurns; rounds++ {
		log.Write(memory)
		response, err := a.llm.GetCompletion(ctx, memory)
		if err != nil {
			return err
		}

		memory = append(memory, response)
		log.Write(memory)

		toolExecuted := false
		for _, tool := range a.tools {
			expression := tool.Check()
			re := regexp.MustCompile(expression)
			matches := re.FindStringSubmatch(response.Text)

			if len(matches) > 1 {
				toolResponse, err := tool.Run(matches[1:]...)
				if err != nil {
					toolResponse = fmt.Sprintf("Could not run the tool. Error is %s. Could you please fix?", err)
				}
				memory = append(memory, &llm.Message{
					Role: "user",
					Text: fmt.Sprintf("Tool Response is: \n%s\n----\n. Please use the JSON response format as defined.", toolResponse),
				})

				toolExecuted = true
				break
			}
		}

		if toolExecuted {
			continue
		}

		jsonResponse, err := llm.ParseJSONResponse[prompt.ResponseStruct](response.Text)
		if err != nil {
			memory = append(memory, &llm.Message{
				Role: "user",
				Text: fmt.Sprintf("could not parse response %s", err),
			})
			continue
		}

		errorInStep := false
		for _, step := range a.steps {
			err := step.Execute(jsonResponse)
			if err != nil {
				errorInStep = true
				memory = append(memory, &llm.Message{
					Role: "user",
					Text: fmt.Sprintf("Could not run the code. Error is %s. Could you please correct and follow the same response format (JSON) as before.", err),
				})
				break
			}
		}

		if !errorInStep {
			return nil
		}
	}

	return fmt.Errorf("could not complete agent in %d steps", maxTurns)
}
