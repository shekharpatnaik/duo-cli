package agent

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"

	"github.com/fatih/color"
	"gitlab.com/duo/pkg/llm"

	markdown "github.com/MichaelMure/go-term-markdown"
)

type chatAgent struct {
	llm    llm.LLM
	tools  []Tool
	format bool
}

func NewChatAgent(llm llm.LLM, tools []Tool, format bool) *chatAgent {
	return &chatAgent{
		llm:    llm,
		tools:  tools,
		format: format,
	}
}

func (a *chatAgent) Run(ctx context.Context, startingPrompt string) error {
	memory := []*llm.Message{
		{
			Role: "system",
			Text: startingPrompt,
		},
	}

	reader := bufio.NewReader(os.Stdin)

	toolExecuted := false

	for {
		if !toolExecuted {
			fmt.Printf(color.HiYellowString("User : "))
			input, err := reader.ReadString('\n')
			if err != nil {
				if errors.Is(err, io.EOF) {
					break
				}
				return err
			}

			if strings.Trim(input, "\n") == "" || input == "exit" {
				break
			}

			memory = append(memory, &llm.Message{
				Role: "user",
				Text: input,
			})
		}

		response, err := a.llm.GetCompletion(ctx, memory)
		if err != nil {
			return err
		}
		memory = append(memory, response)

		color.HiYellow("Assistant :")
		if a.format {
			fmt.Printf("%s\n", markdown.Render(string(response.Text), 80, 6))
		} else {
			fmt.Printf("%s\n", response.Text)
		}

		toolExecuted = false
		for _, tool := range a.tools {
			expression := tool.Check()
			re := regexp.MustCompile(expression)
			matches := re.FindStringSubmatch(response.Text)

			if len(matches) > 1 {
				toolResponse, err := tool.Run(matches[1:]...)
				if err != nil {
					toolResponse = fmt.Sprintf("Could not run the tool. Error is %s. Could you please fix?", err)
				}
				memory = append(memory, &llm.Message{
					Role: "user",
					Text: fmt.Sprintf("Tool Response is: \n%s\n----\n.", toolResponse),
				})

				color.HiYellow("System :")
				fmt.Println(toolResponse)

				toolExecuted = true
			}
		}
	}

	return nil
}
