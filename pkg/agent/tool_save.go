package agent

import (
	"fmt"
	"os"

	"gitlab.com/duo/pkg/prompt"
)

type saveFileTool struct{}

func NewSaveFileTool() Tool {
	return &saveFileTool{}
}

func (t *saveFileTool) Check() string {
	return `(?ms)RUN TOOL WRITE (.+)\n\x60\x60\x60(?:[^\n]*\n)?(.+?)\x60\x60\x60`
}

func (t *saveFileTool) Description() string {
	return prompt.SaveToolPrompt
}

func (t *saveFileTool) Run(params ...string) (string, error) {
	err := os.WriteFile(params[0], []byte(params[1]), os.ModePerm)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Successfully wrote file %s", params[0]), nil
}
