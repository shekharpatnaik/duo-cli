package agent

import (
	"fmt"
	"os/exec"
	"strings"

	"gitlab.com/duo/pkg/prompt"
)

type executeCommand struct{}

func NewExecuteCommandStep() PipelineStep {
	return &executeCommand{}
}

func (s *executeCommand) Execute(response *prompt.ResponseStruct) error {
	// Get filepath and response dir
	commands := strings.Split(response.Command, " ")
	res, err := exec.Command(commands[0], commands[1:]...).CombinedOutput()
	if err != nil {
		return fmt.Errorf("error in output of running command %s: Error %s\n %s", response.Command, err, res)
	}

	return nil
}
