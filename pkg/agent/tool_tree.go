package agent

import (
	"gitlab.com/duo/pkg/fs"
	"gitlab.com/duo/pkg/prompt"
)

type treeTool struct{}

func NewTreeTool() Tool {
	return &treeTool{}
}

func (t *treeTool) Check() string {
	return "RUN TOOL TREE"
}

func (t *treeTool) Description() string {
	return prompt.TreeToolPrompt
}

func (t *treeTool) Run(params ...string) (string, error) {
	res, err := fs.GenerateTree(".")
	if err != nil {
		return "", err
	}
	return res, nil
}
