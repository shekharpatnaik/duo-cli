package agent

import "gitlab.com/duo/pkg/prompt"

type PipelineStep interface {
	Execute(response *prompt.ResponseStruct) error
}
