package agent

import (
	"os"
	"path"

	"gitlab.com/duo/pkg/prompt"
)

type writeFile struct {
	response string
}

func NewWriteFileStep() PipelineStep {
	return &writeFile{}
}

func (s *writeFile) Execute(response *prompt.ResponseStruct) error {
	// Write File
	err := os.MkdirAll(path.Dir(response.Filename), os.ModePerm)
	if err != nil {
		return err
	}

	err = os.WriteFile(response.Filename, []byte(response.Contents), os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}
