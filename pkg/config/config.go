package config

import (
	"errors"
	"fmt"
	"os"
	"path"
	"strconv"

	"github.com/fatih/color"
	"github.com/rodaine/table"
	"gopkg.in/yaml.v3"
)

type Config struct {
	LLM         string  `yaml:"llm"`
	ApiKey      string  `yaml:"api_key"`
	Model       string  `yaml:"model"`
	Temperature float32 `yaml:"temperature"`
	Project     string  `yaml:"project"`
	InstanceUrl string  `yaml:"instance_url"`
}

func getConfigPath(profile string) (string, error) {
	// Find home dir
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return "", err
	}

	configPath := path.Join(homeDir, ".config", "duo", fmt.Sprintf("%s_profile.yaml", profile))
	return configPath, nil
}

func load(profile string) (*Config, error) {
	configPath, err := getConfigPath(profile)
	if err != nil {
		return nil, err
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		err = os.MkdirAll(path.Dir(configPath), os.ModePerm)
		if err != nil {
			return nil, err
		}

		return &Config{}, nil
	}

	data, err := os.ReadFile(configPath)
	if err != nil {
		return nil, err
	}

	var config Config
	err = yaml.Unmarshal(data, &config)

	return &config, err
}

func save(profile string, cfg *Config) error {
	configPath, err := getConfigPath(profile)
	if err != nil {
		return err
	}

	data, err := yaml.Marshal(cfg)
	if err != nil {
		return err
	}

	err = os.WriteFile(configPath, data, 0644)
	if err != nil {
		return err
	}

	return nil
}

func Get(profile string) (*Config, error) {
	return load(profile)
}

func Set(profile string, key string, value string) error {
	config, err := load(profile)
	if err != nil {
		return err
	}

	switch key {
	case "llm":
		config.LLM = value
		break
	case "api_key":
		config.ApiKey = value
		break
	case "model":
		config.Model = value
		break
	case "temperature":
		temp, err := strconv.ParseFloat(value, 32)
		if err != nil {
			return err
		}
		if temp < 0 || temp > 1 {
			return errors.New("temperature should be between 0 and 1")
		}
		config.Temperature = float32(temp)
		break
	case "project":
		config.Project = value
		break
	case "instance_url":
		config.InstanceUrl = value
		break
	default:
		return fmt.Errorf("Invalid key %s", key)
	}

	return save(profile, config)
}

func List(profile string) error {

	configList := map[string]string{
		"llm":          "The LLM to use can be either openai or vertex",
		"api_key":      "The API Key for OpenAI or Anthropic",
		"model":        "The name of the model to use for the LLM",
		"temperature":  "Determines how deterministic are the responses, a value from 0 to 1.",
		"project":      "The GitLab Project ID or in the case of Vertex, the GCP project to use",
		"instance_url": "The instance URL to use for GitLab",
	}

	config, err := load(profile)
	if err != nil {
		return err
	}

	headerFmt := color.New(color.FgGreen, color.Underline).SprintfFunc()
	columnFmt := color.New(color.FgYellow).SprintfFunc()

	tbl := table.New("Key", "Value", "Description")
	tbl.WithHeaderFormatter(headerFmt).WithFirstColumnFormatter(columnFmt)

	for name, description := range configList {
		tbl.AddRow(name, config.getValue(name), description)
	}

	tbl.Print()

	return nil
}

func (cfg *Config) getValue(name string) string {
	switch name {
	case "llm":
		return cfg.LLM
	case "api_key":
		if cfg.ApiKey != "" {
			return "******"
		}

		return ""
	case "model":
		return cfg.Model
	case "temperature":
		return strconv.FormatFloat(float64(cfg.Temperature), 'f', 2, 32)
	case "project":
		return cfg.Project
	case "instance_url":
		return cfg.InstanceUrl
	default:
		return ""
	}
}
