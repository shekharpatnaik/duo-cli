package fs

import (
	"fmt"
	"os"
)

var IgnoreDirectories = map[string]struct{}{
	".git":           {},
	".idea":          {},
	".vscode":        {},
	"node_modules":   {},
	"venv":           {},
	"vendor":         {},
	".DS_Store":      {},
	".gitignore":     {},
	".gitattributes": {},
	"__pycache__":    {},
}

func GenerateTree(root string) (result string, err error) {
	res, err := generateTreeRec(root, 1)
	if err != nil {
		return "", err
	}
	return "root" + res, nil
}

func generateTreeRec(path string, level int) (result string, err error) {
	files, err := os.ReadDir(path)
	if err != nil {
		return result, err
	}
	for _, entry := range files {
		if _, ok := IgnoreDirectories[entry.Name()]; ok {
			continue
		}

		result += fmt.Sprintf("\n%v|-%v", multiplyElement("  ", level), entry.Name())
		if entry.IsDir() {
			r, err := generateTreeRec(path+"/"+entry.Name(), level+1)
			if err != nil {
				return result, err
			}
			result += r
		}
	}
	return result, err
}

func multiplyElement(element string, count int) string {
	result := ""
	for i := 0; i < count; i++ {
		result += element
	}
	return result
}
