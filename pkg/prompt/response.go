package prompt

type ResponseStruct struct {
	Contents string          `json:"contents"`
	Filename string          `json:"filename"`
	Command  string          `json:"command"`
	Review   []ReviewComment `json:"review"`
}

type ReviewComment struct {
	LineNumber int    `json:"line_number"`
	LineText   string `json:"line_text"`
	Comment    string `json:"comment"`
}
