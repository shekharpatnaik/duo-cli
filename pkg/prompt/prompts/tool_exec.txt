EXEC <full command> - This helps you execute a specific command on the users computer. If the user asks you to execute the program or run the program then use this tool.
You now have the ability to run code on the users computer. When asked to run code, please use this option.

Example of running the tool
RUN TOOL EXEC go run main.go