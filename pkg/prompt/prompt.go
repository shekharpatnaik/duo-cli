package prompt

import (
	"bytes"
	_ "embed"
	"text/template"
)

//go:embed prompts/unittest_create.txt
var UnitTestCreatePrompt string

//go:embed prompts/review.txt
var ReviewPrompt string

//go:embed prompts/explain.txt
var ExplainPrompt string

//go:embed prompts/convert.txt
var ConvertPrompt string

//go:embed prompts/gen_doc.txt
var GenDocPrompt string

//go:embed prompts/generate.txt
var GeneratePrompt string

//go:embed prompts/tools.txt
var ToolsPrompt string

//go:embed prompts/tool_exec.txt
var ExecToolPrompt string

//go:embed prompts/tool_read.txt
var ReadToolPrompt string

//go:embed prompts/tool_save.txt
var SaveToolPrompt string

//go:embed prompts/tool_tree.txt
var TreeToolPrompt string

//go:embed prompts/chat_tools.txt
var ChatToolsPrompt string

//go:embed prompts/draw.txt
var DrawPrompt string

func Generate(prompt string, data map[string]string) (string, error) {
	t, err := template.New("prompt").Parse(prompt)
	if err != nil {
		return "", err
	}

	res := bytes.NewBuffer([]byte(""))

	err = t.Execute(res, data)
	if err != nil {
		return "", err
	}

	return res.String(), nil
}
